<?php

namespace App\DataFixtures;

use App\Entity\DrinkType;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;



/**
 * Generate random 20 Users with the ROLE_USER and 3 Users with the ROLE_MANAGER
 */
class DrinkTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $alcools = ['Vodka' => 0.2, 'Jägermeister' => 0.35, 'Whisky' => 0.2, 'Ricard' => 0.25, 'Bière' => 0.05, 'Champagne' => 0.125, 'Rhum' => 0.4];
        foreach ($alcools as $alcool => $degree) {
            $drinkType = new DrinkType();
            $drinkType->setDegree($degree);
            $drinkType->setName($alcool);
            $manager->persist($drinkType);
        }
        $manager->flush();
    }
}