<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProfileRepository::class)
 */
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $sex;

    /**
     * @Assert\Range(
     *      min = 25,
     *      max = 500,
     *      notInRangeMessage = "Votre poids doit faire au moins {{ min }}kg et ne doit pas dépasser {{ max }}kg ATTENTION : faire boire un enfant ou donner de l'alcool à un éléphant est fortement déconseillé !!!",
     * )
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @ORM\OneToMany(targetEntity=CurrentDrink::class, mappedBy="profile", cascade={"persist", "remove"})
     */
    private $currentDrinks;



    // public function __construct()
    // {
    //     $this->currentDrinks = new ArrayCollection();
    // }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSex(): ?float
    {
        return $this->sex;
    }

    public function setSex(float $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    // public function getEat(): ?int
    // {
    //     return $this->eat;
    // }

    // public function setEat(int $eat): self
    // {
    //     $this->eat = $eat;

    //     return $this;
    // }

    /**
     * @return Collection|CurrentDrink[]
     */
    public function getCurrentDrinks(): Collection
    {
        return $this->currentDrinks;
    }

    public function addCurrentDrink(CurrentDrink $currentDrink): self
    {
        if (!$this->currentDrinks->contains($currentDrink)) {
            $this->currentDrinks[] = $currentDrink;
            $currentDrink->setProfile($this);
        }

        return $this;
    }

    public function removeCurrentDrink(CurrentDrink $currentDrink): self
    {
        if ($this->currentDrinks->removeElement($currentDrink)) {
            // set the owning side to null (unless already changed)
            if ($currentDrink->getProfile() === $this) {
                $currentDrink->setProfile(null);
            }
        }

        return $this;
    }


}
