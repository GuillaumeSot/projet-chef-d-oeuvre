<?php

namespace App\Entity;

use App\Repository\CurrentDrinkRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CurrentDrinkRepository::class)
 */
class CurrentDrink
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=DrinkType::class, inversedBy="currentDrinks")
     */
    private $drink_type;

    /**
     * @Assert\Range(
     *      min = 1,
     *      max = 501,
     *      notInRangeMessage = "Mettez au moins {{ min }} cl dans votre verre, et celui-ci ne dois pas non plus faire 150  litres",
     * )
     * @ORM\Column(type="integer")
     */
    private $cl;

    /**
     * @ORM\Column(type="float")
     */
    private $alcohol_blood_g;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="currentDrinks")
     */
    private $profile;

    /**
     * @ORM\Column(type="integer")
     */
    private $eat;

    /**
     *  @Assert\Type(
     *     type="float",
     *     message="The value {{ value }} is not a valid {{ type }}.",
     * )
     * @Assert\Range(
     *      min = 0.1,
     *      max = 99,
     *      notInRangeMessage = "Les degrés d'alcool doit être compris entre {{ min }}° et {{ max }}°",
     * )
     * @ORM\Column(type="float")
     */
    private $degree;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getDrinkType(): ?DrinkType
    {
        return $this->drink_type;
    }

    public function setDrinkType(?DrinkType $drink_type): self
    {
        $this->drink_type = $drink_type;

        return $this;
    }

    public function getCl(): ?int
    {
        return $this->cl;
    }

    public function setCl(int $cl): self
    {
        $this->cl = $cl;

        return $this;
    }

    public function getAlcoholBloodG(): ?float
    {
        return $this->alcohol_blood_g;
    }

    public function setAlcoholBloodG(float $alcohol_blood_g): self
    {
        $this->alcohol_blood_g = $alcohol_blood_g;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getEat(): ?int
    {
        return $this->eat;
    }

    public function setEat(int $eat): self
    {
        $this->eat = $eat;

        return $this;
    }

    public function getDegree(): ?float
    {
        return $this->degree;
    }

    public function setDegree(float $degree): self
    {
        $this->degree = $degree;

        return $this;
    }
}
