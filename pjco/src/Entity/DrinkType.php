<?php

namespace App\Entity;

use App\Repository\DrinkTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=DrinkTypeRepository::class)
 */
class DrinkType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\Range(
     *      min = 0.1,
     *      max = 99,
     *      notInRangeMessage = "Les degrés d'alcool doit être compris entre {{ min }}° et {{ max }}°",
     * )
     * @ORM\Column(type="float")
     */
    private $degree;

    /**
     * @ORM\OneToMany(targetEntity=CurrentDrink::class, mappedBy="drink_type", cascade={"persist", "remove"} )
     */
    private $currentDrinks;

    public function __construct()
    {
        $this->currentDrinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDegree(): ?float
    {
        return $this->degree;
    }

    public function setDegree(float $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * @return Collection|CurrentDrink[]
     */
    public function getCurrentDrinks(): Collection
    {
        return $this->currentDrinks;
    }

    public function addCurrentDrink(CurrentDrink $currentDrink): self
    {
        if (!$this->currentDrinks->contains($currentDrink)) {
            $this->currentDrinks[] = $currentDrink;
            $currentDrink->setDrinkType($this);
        }

        return $this;
    }

    public function removeCurrentDrink(CurrentDrink $currentDrink): self
    {
        if ($this->currentDrinks->removeElement($currentDrink)) {
            // set the owning side to null (unless already changed)
            if ($currentDrink->getDrinkType() === $this) {
                $currentDrink->setDrinkType(null);
            }
        }

        return $this;
    }
}
