<?php

namespace App\Form;

use App\Entity\DrinkType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DrinkTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class ,['label' => 'Nom'])
            ->add('degree',NumberType::class ,['label' => 'Degrés',
            'invalid_message' => 'Veuiller saisir un chiffre ou un nombre',
            'attr' => array(
                'placeholder' => 'De 0.1 à 99'
            ),])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DrinkType::class,
        ]);
    }
}
