<?php

namespace App\Form;

use App\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weight',IntegerType::class,[
                'label' => 'Poids',
                'attr' => array(
                    'placeholder' => 'Inscrivez votre poids (en KG)'
                ),
            ])
            ->add('sex', ChoiceType::class, [
                'choices' => [
                        'Homme' => 0.7,
                        'Femme' => 0.6,
                ],
                'label' => 'Sexe',
                'expanded' => true, 
                'multiple' => false,
                    
                ]
                        )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }
}
