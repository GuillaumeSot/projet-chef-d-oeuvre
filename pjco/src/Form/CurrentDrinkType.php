<?php

namespace App\Form;

use App\Entity\CurrentDrink;
use App\Entity\DrinkType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;



class CurrentDrinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sessEat= $options['sessEat'];
        $builder
            ->add('drink_type',  EntityType::class, [
                'class' => DrinkType::class,
                'choice_label' => 'name',
                'label' => "Type d'alcool",
            ])
            ->add('cl',NumberType::class ,['label' => 'Quantité versé (cl)',
            'attr' => array(
                'placeholder' => 'Quantité en centilitre'
            ),
            'invalid_message' => 'Veuiller saisir un chiffre ou un nombre'])
            ->add('degree',NumberType::class ,['label' => 'Nombre de degrés',
            'invalid_message' => 'Veuiller saisir un chiffre ou un nombre',
            'attr' => array(
                'placeholder' => 'De 0.1 à 99'
            ),])
            ->add('date', ChoiceType::class, [
                'label' => "Depuis combiens de temps avez vous bu votre verre ?",
                'choices' => [
                    'Maintenant' => 'PT0H',
                    '15min' => 'PT0H15M',
                    '30min' => 'PT0H30M',
                    '45min' => 'PT0H45M',
                    '1h00' => 'PT1H',
                    '1h30' => 'PT1H30M',
                    '2h00' => 'PT2H'
            ],
            'mapped' => false
            ])
            ->add('eat' , ChoiceType::class, [
                'choices' => [
                        'Oui' => 60,
                        'Non' => 30,
                ],
                'expanded' => true, 
                'multiple' => false,
                'data' => $sessEat,
                'label' => 'Avez-vous mangez moins de 2h avant ce verre ?'
                ]
                )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // 'data_class' => CurrentDrink::class,
            'sessEat'=>60,
        ]);
        // $resolver->setRequired(['a']);
    }
}