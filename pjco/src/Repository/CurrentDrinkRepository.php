<?php

namespace App\Repository;

use App\Entity\CurrentDrink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CurrentDrink|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrentDrink|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrentDrink[]    findAll()
 * @method CurrentDrink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrentDrinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CurrentDrink::class);
    }

    // /**
    //  * @return CurrentDrink[] Returns an array of CurrentDrink objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CurrentDrink
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @return CurrentDrink[] Returns an array of Profile objects
     */
    public function findCurrentDrinkByProfile($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.profile = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            // ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
}
