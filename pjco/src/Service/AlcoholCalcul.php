<?php 
namespace App\Service;

class AlcoholCalcul{

    public function getAlcoholBloodGramByDrinks($profile,$degree,$cl){
       $ml =  $cl * 10;
       $weight = $profile->getWeight();
       $sex = $profile->getSex();
       $alcoholBlood = ($ml*$degree*0.8)/($weight*$sex) ;
       return $alcoholBlood;

    }
    public function getAlcoholInTheTime($profileCurrentDrink ,$sex): float
    {

        $tab=[] ;

        function convertSex($a) {
            if($a === 0.6){
                return 0.1;
            }
            else{
                return 0.15;
            }
            
        };
        foreach($profileCurrentDrink as $profileCurrentDrinks){

            $eat = $profileCurrentDrinks->getEat();
            $BloodG = $profileCurrentDrinks->getAlcoholBloodG();
            $dateNow = new \DateTime('now');
            $dateDrink = $profileCurrentDrinks->getCreatedAt();

            // calcul how many minutes the drink is finish 
            $minutes = ($dateNow->getTimestamp() - $dateDrink->getTimestamp())/60;


            if($minutes >= $eat){
                // 0.7 to 0.15 or 0.6 to 0.1 by hour
                $eliminAlco = convertSex($sex) ;

                //how many alcohol elimin by minutes 
                $eliminMin = $eliminAlco/60 ;

                //calcul how many time the alcohol down 
                $eliminG = ($minutes - $eat)* $eliminMin;

                // sub alcohol elimin at alcohol of the drink 
                $drinkG = $BloodG - $eliminG;

                //if under 0 , do nothing 
                if($drinkG < 0){
                    $drinkG = 0 ;
                }

                //put result in tab 
                else{
                    $tab[]=$drinkG ;
                }
            }

            // if minutes < eatTime
            else{
                // Devide alcohol gram of drink by if eat(30 or 60 )
                $UpG = ($BloodG/$eat)*$minutes;
                //put in tab 
                $tab[]= $UpG;
            }
        }
            //arrondie à 3 chiffres après la virgule and calculate the sum of all alcohol gram drinks 
            $totalG = round(array_sum($tab), 3);
            return $totalG;
    }

    
    public function getAllAlcoholGram($profileCurrentDrink ,$sex)
    {
        if($profileCurrentDrink != null){
        $tabGram=[] ;
        $tabDate=[];
        $tab = [];
        function sex($a) {
            if($a === 0.6){
                return 0.1;
            }
            else{
                return 0.15;
            }
        }

        //get all date drink
        $tabAllDate=[];
        foreach($profileCurrentDrink as $profileCurrentDrinks){
            $date= $profileCurrentDrinks->getCreatedAt();
            $tabAllDate[]=$date;
         }

         //get min date and max date
        $begin=min($tabAllDate);
        $endDate=max($tabAllDate);

        
        $dateEnd = clone $endDate;
        $lastDrinkMin = clone $endDate;
        //last date drinks + 30 min
        $lastDrinkMin->add(new \DateInterval('PT0H30M'));
        //last date + infinite days
        $end = $dateEnd->add(new \DateInterval('P1000D'));

        //loop on begin date drinks to infinite days
        $interval = new \DateInterval('PT0H1M');
        $daterange = new \DatePeriod($begin, $interval ,$end);
            foreach($daterange as $date){

                // if date > last drink date + 30 min and last value tab is 0 break loop
                if($date > $lastDrinkMin && end($tabGram) == 0) {
                    break;
                }   
                
                foreach($profileCurrentDrink as $profileCurrentDrinks){
                    $eat = $profileCurrentDrinks->getEat();
                    $BloodG = $profileCurrentDrinks->getAlcoholBloodG();
                    
                    $dateNow = $date;
                    $dateDrink = $profileCurrentDrinks->getCreatedAt();
                    $minutes = ( $dateNow->getTimestamp() - $dateDrink->getTimestamp())/60;
                    if($minutes >= $eat){
                        $eliminAlco = sex($sex) ;
                        $eliminMin = $eliminAlco/60 ;
                        $eliminG = ($minutes - $eat)* $eliminMin;
                        $drinkG = $BloodG - $eliminG;
                        if($drinkG <= 0){
                            $drinkG = 0 ;
                        }
                        else{
                            $tab[]=$drinkG ;
                    
                        }
                    }
                    elseif($minutes<= 0) {
                            $tab[]=0;             }
                    else{

                        $UpG = ($BloodG/$eat)*$minutes;
                        $tab[]= $UpG;
                    }
                }
                $totalG = round(array_sum($tab), 3);
                $tab = [];
                $tabGram[] = $totalG;
                $strDate = date_format($date, 'H:i');
                $tabDate[] =  $strDate;
                    }   
                    
                
                function findValueIndex($tab, $value){
                    $i = -1;
                    $tabIndex=[];
                    //loop on array
                    foreach($tab as $tabs){
                        
                        $i++;
                        if($i > 0){
                            $mi = $i-1;

                        // si la valeur actuel du tableau est plus petite ou égale à la valeur de la fonction
                        //et que la valeur précédente est plus grande     
                        if($tabs <= $value && $tab[$mi] > $value){
                            $tabIndex[]=$i;
                        }
                    }
                }
                return end($tabIndex);
            }
                    $index02 = findValueIndex($tabGram,0.2);
                    $index05 = findValueIndex($tabGram,0.5);
                    $index08 = findValueIndex($tabGram,0.8);
                    $indexMax = array_search(max($tabGram), $tabGram); 
                    

                    
                    $tGram = json_encode($tabGram);
                    $tDate = json_encode($tabDate);

                    $countTab = count($tabGram)-1;
                    function ifNull($v){
                        if($v == NULL){
                            return false;
                        }
                        else{
                            return $v;

                        }
                    }
                    $index02 = ifNull($index02);
                    $index05 = ifNull($index05);
                    $index08 = ifNull($index08);
                    $indexMax = ifNull($indexMax);
                    $countTab = ifNull($countTab);
                    $tGram = ifNull($tGram);
                    $tDate = ifNull($tDate);

                                     
                    $total = [$tabGram,$tabDate,$indexMax,$tGram,$tDate,$index02,$index05,$index08,$countTab];
                    return $total;
    
                    }
                else{
                    return $total=[false,false,false,0,0,false,false,false];
                }    
            }


            public function setAllAlcoholDrinksInTab($profileCurrentDrink)
            {
                $tab=[];
                foreach($profileCurrentDrink as $profileCurrentDrinks){
                    $tab[] = $profileCurrentDrinks->getId();
                };
                return $tab;

            }
        }