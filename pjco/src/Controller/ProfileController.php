<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\DrinkType;
use App\Entity\CurrentDrink;
use App\Form\ProfileType;
use App\Form\CurrentDrinkType;
use App\Repository\ProfileRepository;
use App\Repository\CurrentDrinkRepository;
use App\Service\AlcoholCalcul;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

// use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{

    /**
     * @Route("/", name="profile_new", methods={"GET","POST"})
     */
    public function new(Request $request , SessionInterface $session): Response
    {
        $profile = new Profile();
  
        $form = $this->createForm(ProfileType::class, $profile );
        $form->handleRequest($request);

       
        

        if ($form->isSubmitted() && $form->isValid()) {
         
   
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($profile);
            $entityManager->flush();
         
            $session->set('profile', $profile);
            $session->set('eat', 60);
            return $this->redirectToRoute('profile_drink', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('profile/new.html.twig', [
            'profile' => $profile,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/mydrink", name="profile_drink", methods={"GET","POST"})
     */
    public function myDrink(Request $request, CurrentDrinkRepository $currentDrinkRepository, SessionInterface $session, AlcoholCalcul $alcoholCalcul, ProfileRepository $profileRepository): Response
    {  
        //Verify if profile exist in session
        if($session->get('profile') != NULL){

                //Dql request for recup profile in session
                $profileId = $session->get('profile')->getId();
                $profile = $profileRepository->findOneProfileById($profileId);

                //recup if eat
                $sessEat = $session->get('eat');

                $currentDrink = new CurrentDrink();

                $form = $this->createForm(CurrentDrinkType::class,$currentDrink,['sessEat'=>$sessEat]);
                $form->handleRequest($request);

                //request for search current drink 
                $profileCurrentDrink = $currentDrinkRepository->findCurrentDrinkByProfile($profile);
                $sex = $profile->getSex();

                //calcul gram in blood in real time 
                $total = $alcoholCalcul->getAlcoholInTheTime($profileCurrentDrink,$sex);

                //calcul gram in blood in all time to 0 gram 
                $tabGD = $alcoholCalcul->getAllAlcoholGram($profileCurrentDrink,$sex);

                //add all currentDrink at session
                $tabDrinks = $alcoholCalcul->setAllAlcoholDrinksInTab($profileCurrentDrink);
                $session->set('tabDrinks',$tabDrinks);

                
                if ($form->isSubmitted() && $form->isValid()) {
                    //recup data form 
                    $dataDrink = $form['drink_type']->getData();
                    $dataCreated = $form['date']->getData();
                    $dataEat = $form['eat']->getData();
                    $dataCl = $form['cl']->getData();
                    $dataDegree = $form['degree']->getData();
                    $dataDegreePercent = $dataDegree/100;
                    
                    //calcul AlcoholBloodDrinks
                    $alcoholBlood = $alcoholCalcul->getAlcoholBloodGramByDrinks($profile,$dataDegreePercent,$dataCl) ;
                    
                    
                    //Date now sub , date when he take drinks
                    $now = new \DateTime('now');
                    $now->sub(new \DateInterval($dataCreated));
                    
                    
                    $currentDrink->setCreatedAt($now);
                    $currentDrink->setAlcoholBloodG($alcoholBlood);
                    $currentDrink->setProfile($profile);
                    $currentDrink->setCl($dataCl);
                    $currentDrink->setDrinkType($dataDrink);
                    $currentDrink->setEat($dataEat);
                    $currentDrink->setDegree($dataDegreePercent);

                    //set eat at session
                    $session->set('eat', $dataEat);
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($currentDrink);
                    $entityManager->flush();
            
                    

                    return $this->redirectToRoute('profile_drink', [], Response::HTTP_SEE_OTHER);
                }
                
            

            
            return $this->render('profile/my_drink.html.twig', [
                'form' => $form->createView(),
                'totalG' => $total,
                'profileCurrentDrink' => $profileCurrentDrink,
                'profile' => $profile,
                'tabGD'=> $tabGD,
            ]);
        }
        else{
            return $this->render('profile/end_session.html.twig',[]);
        }
    }



    /**
     * @Route("/edit", name="profile_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SessionInterface $session,ProfileRepository $profileRepository, CurrentDrinkRepository $currentDrinkRepository, AlcoholCalcul $alcoholCalcul): Response
    {
        if($session->get('profile') != NULL){
            $profileId = $session->get('profile')->getId();
            $profile = $profileRepository->findOneProfileById($profileId);
            $profileCurrentDrink = $currentDrinkRepository->findCurrentDrinkByProfile($profile);
                $form = $this->createForm(ProfileType::class, $profile);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $entityManager = $this->getDoctrine()->getManager();
                    $session->set('profile', $profile);
                    foreach($profileCurrentDrink as $profileCurrentDrinks){
                        $degree = $profileCurrentDrinks->getDegree();
                        $cl = $profileCurrentDrinks->getCl();
                        $alcoholBlood = $alcoholCalcul->getAlcoholBloodGramByDrinks($profile,$degree,$cl);
                        $profileCurrentDrinks->setAlcoholBloodG($alcoholBlood);
                        $entityManager->persist($profileCurrentDrinks);
                    }
                    $entityManager->flush();

                    return $this->redirectToRoute('profile_drink', [], Response::HTTP_SEE_OTHER);
                }

                return $this->renderForm('profile/edit.html.twig', [
                    'profile' => $profile,
                    'form' => $form,
                ]);
            }
            else{
                return $this->render('profile/end_session.html.twig',[]);
            }
    }

     /**
     * @Route("/mydrink/json", name="profile_drink_json", methods={"GET"})
     */
    public function api(AlcoholCalcul $alcoholCalcul,CurrentDrinkRepository $currentDrinkRepository, SessionInterface $session, ProfileRepository $profileRepository): Response
    {
        
            //get profile in session 
            $profile = $session->get('profile');
            //request for find profile by id 
            $profile = $profileRepository->findOneBy(['id' => $profile->getId() ]);
            //find current drink by profile 
            $profileCurrentDrink = $currentDrinkRepository->findCurrentDrinkByProfile($profile);

            $sex = $profile->getSex();
            //algo calcul drink in the time 
             $total = $alcoholCalcul->getAlcoholInTheTime($profileCurrentDrink,$sex);
            
            return $this->json(['code' => 200, 'total'=>$total]); 
        
    }

    /**
     * @Route("/my_drink/{id}/edit", name="mydrink_edit", methods={"GET","POST"})
     */
    public function editMyDrink(Request $request, SessionInterface $session,CurrentDrinkRepository $currentDrinkRepository, AlcoholCalcul $alcoholCalcul): Response
    {
            if($session->get('profile') != NULL){
                //recup id url 
                $id = $request->get('id');
                //get current drinks session 
                $tabDrinks = $session->get('tabDrinks');
                $profile = $session->get('profile');
                //get currentDrinks by id url
                $currentDrink = $currentDrinkRepository->findOneBy(['id' => $tabDrinks[$id]]);
                
                $form = $this->createForm(CurrentDrinkType::class, $currentDrink);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    
                    $dataDegree = $form['degree']->getData();
                    $dataCl = $form['cl']->getData();
                    $dataDegreePercent = $dataDegree/100;
                    $alcoholBlood= $alcoholCalcul->getAlcoholBloodGramByDrinks($profile,$dataDegreePercent,$dataCl);
                    $currentDrink->setDegree($dataDegreePercent);
                    $currentDrink->setAlcoholBloodG($alcoholBlood);
                    $this->getDoctrine()->getManager()->flush();

                    return $this->redirectToRoute('profile_drink', [], Response::HTTP_SEE_OTHER);
                }
                

                return $this->renderForm('profile/mydrink_edit.html.twig', [
                    'currentDrink' => $currentDrink,
                    'form' => $form,
                ]);
            }
            else{
                return $this->render('profile/end_session.html.twig',[]);
            }
        }
        // else{
        //     return $this->render('profile/end_session.html.twig',[]);
        // }
    // }

    /**
     * @Route("/my_drink/{id}/delete", name="current_drink_delete", methods={"GET","POST"})
     */
    public function deleteMydDrink(Request $request, CurrentDrinkRepository $currentDrinkRepository,SessionInterface $session): Response
    {
        $id = $request->get('id');
        $tabDrinks = $session->get('tabDrinks');
        $currentDrink = $currentDrinkRepository->findOneBy(['id' => $tabDrinks[$id]]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($currentDrink);
        $entityManager->flush();

        return $this->redirectToRoute('profile_drink', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/end_session", name="end_session", methods={"GET","POST"})
     */
    public function endSession(SessionInterface $session): Response
    {
        $session->clear();

        return $this->redirectToRoute('profile_new', [], Response::HTTP_SEE_OTHER);
    }
    
    /**
     * @Route("/drink_type/json/{id}", name="drink_type_json", methods={"GET"})
     */
    public function drinkJson(DrinkType $drinkType): Response
    {   
        //recup drink degree by id and return a json response
        $degree = $drinkType->getDegree();
        return $this->json(['code' => 200, 'degree'=>$degree]); 
    }

    



}
