<?php

namespace App\Controller;

use App\Entity\DrinkType;
use App\Form\DrinkTypeType;
use App\Repository\DrinkTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/drink_type")
 */
class DrinkTypeController extends AbstractController
{
    /**
     * @Route("/", name="drink_type_index", methods={"GET"})
     */
    public function index(DrinkTypeRepository $drinkTypeRepository): Response
    {
        return $this->render('drink_type/index.html.twig', [
            'drink_types' => $drinkTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="drink_type_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $form = $this->createForm(DrinkTypeType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $drinkType = new DrinkType();
            $dataDegree = $form['degree']->getData();
            $dataDegreePercent = $dataDegree/100;
            $dataName = $form['name']->getData();
            $drinkType->setDegree($dataDegreePercent);
            $drinkType->setName($dataName);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($drinkType);
            $entityManager->flush();
            return $this->redirectToRoute('drink_type_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->renderForm('drink_type/new.html.twig', [
            // 'drink_type' => $drinkType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="drink_type_show", methods={"GET"})
     */
    public function show(DrinkType $drinkType): Response
    {   
        return $this->render('drink_type/show.html.twig', [
            'drink_type' => $drinkType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="drink_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DrinkType $drinkType): Response
    {
        $form = $this->createForm(DrinkTypeType::class, $drinkType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataDegree = $form['degree']->getData();
            $dataDegreePercent = $dataDegree/100;
            $dataName = $form['name']->getData();
            $drinkType->setDegree($dataDegreePercent);
            $drinkType->setName($dataName);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('drink_type_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->renderForm('drink_type/edit.html.twig', [
            'drink_type' => $drinkType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="drink_type_delete", methods={"POST"})
     */
    public function delete(Request $request, DrinkType $drinkType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$drinkType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($drinkType);
            $entityManager->flush();
        }
        return $this->redirectToRoute('drink_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
