<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210906131147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE current_drink (id INT AUTO_INCREMENT NOT NULL, drink_type_id INT DEFAULT NULL, profile_id INT DEFAULT NULL, created_at DATETIME NOT NULL, cl INT NOT NULL, alcohol_blood_g INT NOT NULL, INDEX IDX_A9A037D5E7E8D8A1 (drink_type_id), INDEX IDX_A9A037D5CCFA12B8 (profile_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE drink_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, degree INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, sex VARCHAR(255) NOT NULL, weight INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE current_drink ADD CONSTRAINT FK_A9A037D5E7E8D8A1 FOREIGN KEY (drink_type_id) REFERENCES drink_type (id)');
        $this->addSql('ALTER TABLE current_drink ADD CONSTRAINT FK_A9A037D5CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE current_drink DROP FOREIGN KEY FK_A9A037D5E7E8D8A1');
        $this->addSql('ALTER TABLE current_drink DROP FOREIGN KEY FK_A9A037D5CCFA12B8');
        $this->addSql('DROP TABLE current_drink');
        $this->addSql('DROP TABLE drink_type');
        $this->addSql('DROP TABLE profile');
    }
}
