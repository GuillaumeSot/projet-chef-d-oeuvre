## To install my project, follow these instructions: 

- Go to the folder "pjco"

- Create a .env.local and add your database, Example:
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=13&charset=utf8"

- run : composer install

- run : npm install

- run : npm run build

- run : php bin/console doctrine:fixtures:load

- run : bin/console server:start OR php -S localhost:8001 -t public
**It will run the project on your localhost:8001 port !**

- for open the application you need to go on localhost:8001/profile

- if you want to access at the CRUD of drink type you need to log you as admin. Create user with the path "/register" and change his role in your database on "["ROLE_ADMIN"]"

- for change environement dev on prod you need to go on ".env" line 17 and change "APP_ENV=dev" on "APP_ENV=prod" 


